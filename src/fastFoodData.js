export const fastFoodData = [
  {
    name: "Cheeseburger",
    ingredients: "Beef patty, cheese, lettuce, tomato, ketchup, bun",
    price: 5,
    photo: "foods/cheeseburger.jpg",
    soldOut: false,
  },
  {
    name: "Chicken Nuggets",
    ingredients: "Breaded chicken pieces, dipping sauce",
    price: 4,
    photo: "foods/chicken-nuggets.jpg",
    soldOut: true,
  },
  {
    name: "French Fries",
    ingredients: "Potato fries, salt, ketchup",
    price: 3,
    photo: "foods/french-fries.jpg",
    soldOut: false,
  },
  {
    name: "Hot Dog",
    ingredients: "Sausage, bun, mustard, onions, relish",
    price: 4,
    photo: "foods/hotdog.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Slice",
    ingredients: "Cheese, tomato sauce, pepperoni, crust",
    price: 3.5,
    photo: "foods/pizza.jpg",
    soldOut: false,
  },
  {
    name: "Milkshake",
    ingredients: "Milk, ice cream, flavoring",
    price: 4.5,
    photo: "foods/milkshake.jpg",
    soldOut: true,
  },
];
