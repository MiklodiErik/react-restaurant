import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { fastFoodData } from "./fastFoodData";

function App() {
  return (
    <>
      <Header />
      <main>
        <Menu />
      </main>
      <Footer />
    </>
  );
}

function Header() {
  if (fastFoodData.length > 0) {
    return (
      <header>
        <h1>React Restaurant</h1>
      </header>
    );
  } else {
    return <p>no content.</p>;
  }
}

function Menu() {
  return (
    <div className="menu">
      {fastFoodData.length > 0 ? (
        fastFoodData.map((food) => <Food foodObject={food} key={food.name} />)
      ) : (
        <p className="no-food">No food available!</p>
      )}
    </div>
  );
}

function Food({ foodObject }) {
  return (
    <div className="food">
      <img
        className={`${foodObject.soldOut ? "sold-out-img" : ""}`}
        src={foodObject.photo}
        alt={foodObject.name}
      />
      <h3>
        <FoodName foodObject={foodObject} />
        <FoodPrice foodObject={foodObject} />
      </h3>
      <p>{foodObject.ingredients}</p>
    </div>
  );
}

function FoodName({ foodObject }) {
  return foodObject.soldOut ? <del>{foodObject.name}</del> : foodObject.name;
}

function FoodPrice({ foodObject }) {
  return (
    <span className="price">
      {foodObject.soldOut ? (
        <span className="sold-out-text">SOLD OUT</span>
      ) : (
        <span>{foodObject.price}$</span>
      )}
    </span>
  );
}

function Footer() {
  const currentDate = new Date().toLocaleDateString();
  const currentTime = new Date().getHours();

  const openingHours = {
    open: 12,
    close: 22,
  };

  const restaurantStatus =
    currentTime >= openingHours.open && currentTime < openingHours.close
      ? true
      : false;

  return (
    <footer>
      {currentDate}
      <span className="restaurant-status">
        {restaurantStatus ? "We are open" : "We are closed"}
      </span>

      {false ? <ComponentA /> : <ComponentB />}
    </footer>
  );
}

function ComponentA() {
  return <p>A</p>;
}
function ComponentB() {
  return <p>B</p>;
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);
